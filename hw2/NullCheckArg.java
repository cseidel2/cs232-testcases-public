//this program checks if the union for arguments from different inherited classes takes the greatest upper bound.
class NullCheckArg {
    public static void main(String[] a){
        System.out.println(new A().id(new B()));
    }
}

class A {
    public int id(B arg){
        return arg.id();
    }
}

class B {
    B b;
    public int id(){
        return new A().id(b);
    }
}

class C extends B {
    public int id(){
        return new A().id(new B());
    }
}
