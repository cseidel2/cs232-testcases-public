// Test the recursive function 
class NotNullField {
    public static void main(String[] x) {
        System.out.println(new A().foo());
    }
}

class A {
    A a;
    public int foo() {
        a = new A();
        System.out.println(this.foo());
        return a.foo();
    }
}