/**
 * Tests the case when a field is set to equal to another field in a function call.
 * In this particular example there is null ptr exception.
 */

class Test {
    public static void main(String[] a) {
        A a;
        B b;
        a = new A();
        b = a.m();
    }
}

class A {
    B b1;
    B b2;
    public B m() {
        b1 = new B();
        b1 = b1.m();
        b1 = this.n();
        b1 = b1.m();
        return b1;
    }

    public B n() {
        b1 = b2;
        return b1;
    }
}

class B {
    public B m() {
        return new B();
    }
}


