/*** 
    This example is free of null-pointer error.
    It initates B instB in function initB().
    And then it calls instB in the next statement.
    It tests whether the nullpointerchecker will 
    catch the initiation of instB in function initB()
***/

class Safe
{
    public static void main(String[] a)
    {
        System.out.println(new A().showSecret());
    }
}

class A 
{
    B instB;
    int res;

    public int initB()
    {
    	instB = new B();
    	return 1;
    }
    public int showSecret()
    {
    	res = this.initB();
    	res = instB.secret();
        return res;
    }
}

class B 
{
	int a;
	int b;
	public int secret()
	{
		a = 3;
		b = 5;
		return a + b;
	}
}