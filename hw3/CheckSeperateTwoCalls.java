/*
 ** This testcase will print "The program prints only integers that are greater than zero".
 ** In this testcase, calculate in Class A was called twice. The first call return a negative
 ** number and the second time will return a positive number.
 ** If the analysis is context insensitive, then formalParam of calcualte "num" will be in range [0, 6]
 ** And the res[num - 5] will be [-5, 1]. So when printing r2, r2 may not be a positive number
 ** However, for context sensitive analysis, we create a copy for each call.
 ** Then, the formalParam of calcualte "num" will be in range [6, 6] for the second call.
 ** The result of this call will be in range [1, 1] and r2 = [1,1] when it is printed.
 ** In this case, no non-positive number will be printed.
 **
 */


class CheckSeperateTwoCalls {
    public static void main(String[] a){
        int arg;
        int r1;
        int r2;
        A caller;

        arg = 0;
        caller = new A();
        r1 = caller.calculate(arg);
        arg = 6;
        r2 = caller.calculate(arg);
        System.out.println(r2);
    }
}

class A {
    public int calculate(int num) {
        return num - 5;
    }
}
